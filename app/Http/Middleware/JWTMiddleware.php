<?php

namespace App\Http\Middleware;

use Auth\Application\Interfaces\AuthServiceInterface;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class JWTMiddleware
{
    private AuthServiceInterface $authService;

    public function __construct(
        AuthServiceInterface $authService
    )
    {
        $this->authService = $authService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        try {
            throw_if(
                is_null($request->bearerToken()),
                new \Exception('Token not found', 500)
            );

            $this->authService
                ->loginUserFromToken($request->bearerToken());

        } catch (\Exception $exception) {
            $code = (int) $exception->getCode();
            return response()->json([
                'code' => $code,
                'message' => $exception->getMessage(),
                'success' => false
            ], $code);
        }

        return $next($request);
    }
}
