<?php

namespace App\Providers;

use Auth\Application\Interfaces\AuthServiceInterface;
use Auth\Application\Services\AuthService;
use Auth\Infrastructure\Interfaces\Providers\Services\AuthServiceProviderInterface;
use \Auth\Infrastructure\Providers\Services\AuthServiceProvider;
use Illuminate\Support\ServiceProvider;
use Products\Application\Interfaces\Services\CategoryServiceInterface;
use Products\Application\Interfaces\Services\ProductServiceInterface;
use Products\Application\Services\CategoryService;
use Products\Application\Services\ProductService;
use Products\Infrastructure\Interfaces\Providers\Services\FileServiceProviderInterface;
use Products\Infrastructure\Interfaces\Repositories\Categories\CategoryRepositoryInterface;
use Products\Infrastructure\Interfaces\Repositories\Products\ProductRepositoryInterface;
use Products\Infrastructure\Providers\Services\FileServiceProvider;
use Products\Infrastructure\Repositories\Categories\CategoryRepository;
use Products\Infrastructure\Repositories\Products\ProductRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        // Providers
        $this->app->bind(AuthServiceProviderInterface::class, AuthServiceProvider::class);
        $this->app->bind(FileServiceProviderInterface::class, FileServiceProvider::class);

        // Services
        $this->app->bind(AuthServiceInterface::class, AuthService::class);
        $this->app->bind(CategoryServiceInterface::class, CategoryService::class);
        $this->app->bind(ProductServiceInterface::class, ProductService::class);

        // Repositories
        $this->app->bind(ProductRepositoryInterface::class, ProductRepository::class);
        $this->app->bind(CategoryRepositoryInterface::class, CategoryRepository::class);
    }
}
