<?php

namespace Products\Domain\Dto\Categories;

use Products\Domain\Dto\BaseDto;

class CategoryNewDto extends BaseDto
{
    /**
     * @var string
     */
    public string $name;
}
