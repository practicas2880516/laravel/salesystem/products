<?php

namespace Products\Domain\Dto\Categories;

use Products\Domain\Dto\BaseDto;

class CategoryUpdateDto extends BaseDto
{
    /**
     * @var int
     */
    public int $id;

    /**
     * @var string
     */
    public string $name;
}
