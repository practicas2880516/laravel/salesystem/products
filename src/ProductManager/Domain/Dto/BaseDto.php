<?php

namespace Products\Domain\Dto;

abstract class BaseDto
{
    /**
     * @return array
     */
    public function getAttributes():array
    {
        return array_merge(
            get_class_vars(get_class($this)),
            get_object_vars($this)
        );
    }
}
