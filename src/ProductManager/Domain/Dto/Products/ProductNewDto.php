<?php

namespace Products\Domain\Dto\Products;

use Products\Domain\Dto\BaseDto;

class ProductNewDto extends BaseDto
{
    /**
     * @var string
     */
    public string $name;

    /**
     * @var string
     */
    public string $description;

    /**
     * @var string|null
     */
    public ?string $comments;

    /**
     * @var string
     */
    public string $barcode;

    /**
     * @var float
     */
    public float $price;

    /**
     * @var int
     */
    public int $categoryId;

    /**
     * @var int|null
     */
    public ?int $fileId = null;
}
