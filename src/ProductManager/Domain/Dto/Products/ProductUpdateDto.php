<?php

namespace Products\Domain\Dto\Products;

use Products\Domain\Dto\BaseDto;

class ProductUpdateDto extends BaseDto
{
    /**
     * @var int
     */
    public int $id;

    /**
     * @var string
     */
    public string $name;

    /**
     * @var string
     */
    public string $description;

    /**
     * @var string|null
     */
    public ?string $comments;

    /**
     * @var string
     */
    public string $barcode;

    /**
     * @var float
     */
    public float $price;

    /**
     * @var int
     */
    public int $categoryId;
}
