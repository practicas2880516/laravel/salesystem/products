<?php

namespace Products\Domain\Exceptions;

class ProductNotFoundException extends \Exception
{
    /**
     * @var int
     */
    protected $code = 404;

    /**
     * @var string
     */
    protected $message = 'Product not found';
}
