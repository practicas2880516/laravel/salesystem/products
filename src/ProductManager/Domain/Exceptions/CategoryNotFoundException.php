<?php

namespace Products\Domain\Exceptions;

class CategoryNotFoundException extends \Exception
{
    /**
     * @var int
     */
    protected $code = 404;

    /**
     * @var string
     */
    protected $message = 'Category not found';
}
