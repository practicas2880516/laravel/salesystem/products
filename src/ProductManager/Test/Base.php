<?php

namespace Products\Test;

use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Products\Infrastructure\Interfaces\Repositories\Categories\CategoryRepositoryInterface;
use Products\Infrastructure\Interfaces\Repositories\Products\ProductRepositoryInterface;
use Tests\TestCase;

abstract class Base extends TestCase
{
    /**
     * @var int
     */
    protected int $productId;

    /**
     * @var int
     */
    protected int $categoryId;

    /**
     * @var ProductRepositoryInterface
     */
    protected ProductRepositoryInterface $productRepo;

    /**
     * @var CategoryRepositoryInterface
     */
    protected CategoryRepositoryInterface $categoryRepo;

    /**
     * @var User
     */
    protected User $user;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @return void
     */
    protected function setNewUser():void
    {
        $this->user = new User();
        $this->user->id = 1;
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function createEntityProduct():void
    {
        $this->productId = DB::connection($this->productRepo->getDatabaseConnection())
            ->table($this->productRepo->getTableName())
            ->insertGetId([
                'name' => Str::random(10),
                'description' => Str::random(30),
                'comments' => null,
                'barcode' => Str::random(10),
                'price' => random_int(0, 2000000),
                'category_id' => 1,
                'file_id' => 1,
            ]);
    }

    public function createEntityCategory():void
    {
        $this->categoryId = DB::connection($this->categoryRepo->getDatabaseConnection())
            ->table($this->categoryRepo->getTableName())
            ->insertGetId([
                'name' => Str::random(10),
            ]);
    }

    /**
     * @return void
     */
    protected function setRepositories():void
    {
        $this->productRepo = (App::make(ProductRepositoryInterface::class));
        $this->categoryRepo = (App::make(CategoryRepositoryInterface::class));
    }
}
