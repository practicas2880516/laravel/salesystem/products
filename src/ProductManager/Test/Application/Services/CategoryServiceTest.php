<?php

namespace Products\Test\Application\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Products\Application\Interfaces\Services\CategoryServiceInterface;
use Products\Domain\Dto\Categories\CategoryNewDto;
use Products\Domain\Dto\Categories\CategoryUpdateDto;
use Products\Infrastructure\Interfaces\Repositories\Categories\CategoryRepositoryInterface;
use Products\Infrastructure\Mocks\Repositories\CategoryRepositoryMock;
use Products\Test\Base;

class CategoryServiceTest extends Base
{
    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->setNewUser();
    }

    /**
     * @test
     */
    public function isGetAllWorking()
    {
        $this->instance(
            CategoryRepositoryInterface::class,
            (App::make(CategoryRepositoryMock::class))->generateGetAllWorking()
        );

        $this->actingAs($this->user);

        $categories = (App::make(CategoryServiceInterface::class))
            ->getAll();

        $this->assertNotNull($categories);
    }

    /**
     * @test
     */
    public function isFindByIdWorking()
    {
        $this->instance(
            CategoryRepositoryInterface::class,
            (App::make(CategoryRepositoryMock::class))->generateFindByIdWorking()
        );

        $this->actingAs($this->user);

        $category = (App::make(CategoryServiceInterface::class))
            ->findById(1);

        $this->assertNotNull($category);
    }

    /**
     * @test
     */
    public function isStoreWorking()
    {
        $this->instance(
            CategoryRepositoryInterface::class,
            (App::make(CategoryRepositoryMock::class))->generateStoreWorking()
        );

        $this->actingAs($this->user);

        $dto = new CategoryNewDto();
        $dto->name = Str::random(10);

        $categoryId = (App::make(CategoryServiceInterface::class))
            ->store($dto);

        $this->assertIsInt($categoryId);
    }

    /**
     * @test
     */
    public function isUpdateWorking()
    {
        $this->instance(
            CategoryRepositoryInterface::class,
            (App::make(CategoryRepositoryMock::class))->generateUpdateWorking()
        );

        $this->actingAs($this->user);

        $request = (new Request())->merge([
            'name' => 'Category name'
        ]);

        (App::make(CategoryServiceInterface::class))
            ->update($request, 1);

        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function isUpdateFailing()
    {
        $this->instance(
            CategoryRepositoryInterface::class,
            (App::make(CategoryRepositoryMock::class))->generateUpdateFailing()
        );

        try {
            $this->actingAs($this->user);

            $request = (App::make(Request::class))->merge([
                'name' => 'Category name'
            ]);

            (App::make(CategoryServiceInterface::class))
                ->update($request, 1);

        } catch (\Exception $exception) {
            $this->assertTrue($exception->getCode() === 404);
        }
    }

    /**
     * @test
     */
    public function isUpdateProductWorking()
    {
        $this->instance(
            CategoryRepositoryInterface::class,
            (App::make(CategoryRepositoryMock::class))->generateUpdateProductWorking()
        );

        $this->actingAs($this->user);

        $dto = new CategoryUpdateDto();
        $dto->id = 1;
        $dto->name = Str::random(10);

        (App::make(CategoryServiceInterface::class))
            ->updateProduct($dto);

        $this->assertTrue(true);
    }

}
