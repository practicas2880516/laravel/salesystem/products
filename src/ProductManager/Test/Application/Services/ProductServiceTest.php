<?php

namespace Products\Test\Application\Services;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Products\Application\Interfaces\Services\ProductServiceInterface;
use Products\Domain\Dto\Products\ProductNewDto;
use Products\Domain\Dto\Products\ProductUpdateDto;
use Products\Infrastructure\Interfaces\Providers\Services\FileServiceProviderInterface;
use Products\Infrastructure\Interfaces\Repositories\Products\ProductRepositoryInterface;
use Products\Infrastructure\Mocks\Providers\Services\FileServiceProviderMock;
use Products\Infrastructure\Mocks\Repositories\ProductRepositoryMock;
use Products\Test\Base;

class ProductServiceTest extends Base
{
    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->setNewUser();
    }

    /**
     * @test
     */
    public function  isGetAllWorking()
    {
        $this->instance(
            ProductRepositoryInterface::class,
            (App::make(ProductRepositoryMock::class))->generateGetAllWorking()
        );

        $this->actingAs($this->user);

        $products = (App::make(ProductServiceInterface::class))
            ->getAll();

        $this->assertNotNull($products);
    }

    /**
     * @test
     */
    public function isPaginateWorking()
    {
        $this->instance(
            ProductRepositoryInterface::class,
            (App::make(ProductRepositoryMock::class))->generatePaginateWorking()
        );

        $this->actingAs($this->user);

        $products = (App::make(ProductServiceInterface::class))
            ->paginate(new Request());

        $this->assertNotNull($products);
    }

    /**
     * @test
     */
    public function isStoreWorking()
    {
        $this->instance(
            ProductRepositoryInterface::class,
            (App::make(ProductRepositoryMock::class))->generateStoreProductWorking()
        );

        $this->instance(
            FileServiceProviderInterface::class,
            (App::make(FileServiceProviderMock::class))->generateStoreWorking()
        );

        $this->actingAs($this->user);

        $request = (App::make(Request::class))
            ->merge([
                'name' => 'Product name',
                'description' => 'Product description',
                'comments' => 'Product comments',
                'barcode' => 132456,
                'price' => 2500000,
                'category_id' => 1,
                'file' => UploadedFile::fake()->image('file.png')
            ]);

        $productId = (App::make(ProductServiceInterface::class))
            ->store($request)
            ->getProductId();

        $this->assertIsInt($productId);
    }

    /**
     * @test
     */
    public function isStoreProductTest()
    {
        $this->instance(
            ProductRepositoryInterface::class,
            (App::make(ProductRepositoryMock::class))->generateStoreProductWorking()
        );

        $this->actingAs($this->user);

        $dto = new ProductNewDto();
        $dto->name = 'Product name';
        $dto->description = 'Product description';
        $dto->comments = 'Product comments';
        $dto->barcode = 132456;
        $dto->price = 2500000;
        $dto->categoryId = 1;
        $dto->fileId = 1;

        $productId = (App::make(ProductServiceInterface::class))
            ->storeProduct($dto)
            ->getProductId();

        $this->assertIsInt($productId);
    }

    /**
     * @test
     */
    public function isUpdateWhenNotFileWorking()
    {
        $this->instance(
            ProductRepositoryInterface::class,
            (App::make(ProductRepositoryMock::class))->generateUpdateWorking()
        );

        $this->actingAs($this->user);

        $request = (App::make(Request::class))
            ->merge([
                'name' => 'Product name',
                'description' => 'Product description',
                'comments' => 'Product comments',
                'barcode' => 132456,
                'price' => 2500000,
                'category_id' => 1,
            ]);

        (App::make(ProductServiceInterface::class))
            ->update($request, 1);

        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function isUpdateWhenFileWorking()
    {
        $this->instance(
            ProductRepositoryInterface::class,
            (App::make(ProductRepositoryMock::class))->generateUpdateWorking()
        );

        $this->instance(
            FileServiceProviderInterface::class,
            (App::make(FileServiceProviderMock::class))->generateUpdateWorking()
        );

        $this->actingAs($this->user);

        $request = (App::make(Request::class))
            ->merge([
                'name' => 'Product name',
                'description' => 'Product description',
                'comments' => 'Product comments',
                'barcode' => 132456,
                'price' => 2500000,
                'category_id' => 1,
                'file' => UploadedFile::fake()->image('test.png')
            ]);

        (App::make(ProductServiceInterface::class))
            ->update($request, 1);

        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function isUpdateFailing()
    {
        $this->instance(
            ProductRepositoryInterface::class,
            (App::make(ProductRepositoryMock::class))->generateUpdateFailing()
        );

        try {
            $this->actingAs($this->user);

            $request = (App::make(Request::class))
                ->merge([
                    'name' => 'Product name',
                    'description' => 'Product description',
                    'comments' => 'Product comments',
                    'barcode' => 132456,
                    'price' => 2500000,
                    'category_id' => 1,
                ]);

            (App::make(ProductServiceInterface::class))
                ->update($request, 1);

        } catch (\Exception $exception) {
            $this->assertTrue($exception->getCode() === 404);
        }

    }

    /**
     * @test
     */
    public function isUpdateProductWorking()
    {
        $this->instance(
            ProductRepositoryInterface::class,
            (App::make(ProductRepositoryMock::class))->generateUpdateProductWorking()
        );

        $this->actingAs($this->user);

        $dto = new ProductUpdateDto();
        $dto->id = 1;
        $dto->name = 'Product name';
        $dto->description = 'Product description';
        $dto->comments = 'Product comments';
        $dto->barcode = 132456;
        $dto->price = 2500000;
        $dto->categoryId = 1;

        (App::make(ProductServiceInterface::class))
            ->updateProduct($dto);

        $this->assertTrue(true);
    }
}
