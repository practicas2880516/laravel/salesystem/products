<?php

namespace Products\Test\Infrastructure\Repositories;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Str;
use Products\Domain\Dto\Products\ProductNewDto;
use Products\Domain\Dto\Products\ProductUpdateDto;
use Products\Test\Base;

class ProductRepositoryTest extends Base
{
    use DatabaseTransactions;

    protected function setUp(): void
    {
        parent::setUp();
        $this->setNewUser();
        $this->setRepositories();
        $this->beginDatabaseTransaction();
    }

    /**
     * @test
     */
    public function isFindByIdWorking()
    {
        $this->createEntityProduct();

        $product = $this->productRepo
            ->setUser($this->user)
            ->findById($this->productId);

        $this->assertNotNull($product);
    }

    /**
     * @test
     */
    public function isPaginateWorking()
    {
        $this->createEntityProduct();

        $products = $this->productRepo
            ->setUser($this->user)
            ->paginate();

        self::assertNotNull($products);
    }

    /**
     * @test
     */
    public function isGetAllWorking()
    {
        $this->createEntityProduct();

        $products = $this->productRepo
            ->setUser($this->user)
            ->getAll();

        self::assertNotNull($products);
    }

    /**
     * @test
     */
    public function isStoreWorking()
    {
        $dto = new ProductNewDto();
        $dto->name = Str::random(10);
        $dto->description = Str::random(30);
        $dto->comments = null;
        $dto->barcode = Str::random(10);
        $dto->price = random_int(0, 2000000);
        $dto->categoryId = 1;
        $dto->fileId = 1;

        $productId = $this->productRepo
            ->setUser($this->user)
            ->store($dto)
            ->getProductId();

        $this->assertDatabaseHas($this->productRepo->getTableName(), [
            'id' => $productId,
            'name' => $dto->name,
            'description' => $dto->description,
            'comments' => $dto->comments,
            'barcode' => $dto->barcode,
            'price' => $dto->price,
            'category_id' => $dto->categoryId,
            'file_id' => $dto->fileId,
        ], $this->productRepo->getDatabaseConnection());
    }

    /**
     * @test
     */
    public function isUpdateWorking()
    {
        $this->createEntityProduct();

        $dto = new ProductUpdateDto();
        $dto->id = $this->productId;
        $dto->name = Str::random(10);
        $dto->description = Str::random(30);
        $dto->comments = Str::random(10);
        $dto->barcode = Str::random(10);
        $dto->price = random_int(0, 2000000);
        $dto->categoryId = 1;

        $this->productRepo
            ->setUser($this->user)
            ->update($dto);

        $this->assertDatabaseHas($this->productRepo->getTableName(), [
            'id' => $this->productId,
            'name' => $dto->name,
            'description' => $dto->description,
            'comments' => $dto->comments,
            'barcode' => $dto->barcode,
            'category_id' => $dto->categoryId,
        ], $this->productRepo->getDatabaseConnection());
    }
}
