<?php

namespace Products\Test\Infrastructure\Repositories;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Products\Domain\Dto\Categories\CategoryNewDto;
use Products\Domain\Dto\Categories\CategoryUpdateDto;
use Products\Test\Base;

class CategoryRepositoryTest extends Base
{
    use DatabaseTransactions;

    protected function setUp(): void
    {
        parent::setUp();
        $this->setNewUser();
        $this->setRepositories();
        $this->beginDatabaseTransaction();
    }

    /**
     * @test
     */
    public function isGetAllWorking()
    {
        $this->createEntityCategory();

        $categories = $this->categoryRepo
            ->setUser($this->user)
            ->getAll();

        $this->assertNotNull($categories);
    }

    /**
     * @test
     */
    public function isStoreWorking()
    {
        $dto = new CategoryNewDto();
        $dto->name = 'Nombre categoría uno';

        $categoryId = $this->categoryRepo
            ->setUser($this->user)
            ->store($dto)
            ->getCategoryId();

        $this->assertDatabaseHas($this->categoryRepo->getTableName(), [
            'id' => $categoryId,
            'name' => $dto->name
        ], $this->categoryRepo->getDatabaseConnection());
    }

    /**
     * @test
     */
    public function isUpdateWorking()
    {
        $this->createEntityCategory();

        $dto = new CategoryUpdateDto();
        $dto->id = $this->categoryId;
        $dto->name = 'Nombre categoría uno';

        $this->categoryRepo
            ->setUser($this->user)
            ->update($dto);

        $this->assertDatabaseHas($this->categoryRepo->getTableName(), [
            'id' => $this->categoryId,
            'name' => $dto->name
        ], $this->categoryRepo->getDatabaseConnection());
    }
}
