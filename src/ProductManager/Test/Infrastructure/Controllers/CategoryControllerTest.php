<?php

namespace Products\Test\Infrastructure\Controllers;

use Auth\Application\Interfaces\AuthServiceInterface;
use Auth\Application\Mocks\Services\AuthServiceMock;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Products\Application\Interfaces\Services\CategoryServiceInterface;
use Products\Application\Mocks\Services\CategoryServiceMock;
use Products\Test\Base;

class CategoryControllerTest extends Base
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->setNewUser();

        $this->instance(
            AuthServiceInterface::class,
            (App::make(AuthServiceMock::class))->generateLoginUserFromTokenWorking()
        );

        $this->withHeaders([
            'Authorization' => 'Bearer ' . Str::random(30)
        ]);

    }

    /**
     * @test
     */
    public function isGetAllWorking()
    {
        $this->instance(
            CategoryServiceInterface::class,
            (App::make(CategoryServiceMock::class))->generateGetAllWorking()
        );

        $this->actingAs($this->user);

        $response = $this->get(route('categories.getAll'));

        $response->assertStatus(200);
        $response->assertJsonStructure(['success', 'code', 'message', 'categories']);
    }

    /**
     * @test
     */
    public function isShowWorking()
    {
        $this->instance(
            CategoryServiceInterface::class,
            (App::make(CategoryServiceMock::class))->generateFindByIdWorking()
        );

        $this->actingAs($this->user);

        $response = $this->get(route('categories.show', [1]));

        $response->assertStatus(200);
        $response->assertJsonStructure(['success', 'code', 'message', 'category']);
    }

    /**
     * @test
     */
    public function isStoreWorking()
    {
        $this->instance(
            CategoryServiceInterface::class,
            (App::make(CategoryServiceMock::class))->generateStoreWorking()
        );

        $this->actingAs($this->user);

        $response = $this->post(route('categories.store'), [
            'name' => 'Category name',
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure(['success', 'code', 'message', 'category_id']);
    }

    /**
     * @test
     */
    public function isUpdateWorking()
    {
        $this->instance(
            CategoryServiceInterface::class,
            (App::make(CategoryServiceMock::class))->generateUpdateWorking()
        );

        $this->actingAs($this->user);

        $response = $this->post(route('categories.update', [1]), [
            'name' => 'Category name',
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure(['success', 'code', 'message']);
    }

}
