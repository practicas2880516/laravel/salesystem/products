<?php

namespace Products\Test\Infrastructure\Controllers;

use Auth\Application\Interfaces\AuthServiceInterface;
use Auth\Application\Mocks\Services\AuthServiceMock;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Products\Application\Interfaces\Services\ProductServiceInterface;
use Products\Application\Mocks\Services\ProductServiceMock;
use Products\Test\Base;

class ProductControllerTest extends Base
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->setNewUser();

        $this->instance(
            AuthServiceInterface::class,
            (App::make(AuthServiceMock::class))->generateLoginUserFromTokenWorking()
        );

        $this->withHeaders([
            'Authorization' => 'Bearer ' . Str::random(30)
        ]);

    }

    /**
     * @test
     */
    public function isGetAllWorking()
    {
        $this->instance(
            ProductServiceInterface::class,
            (App::make(ProductServiceMock::class))->generateGetAllWorking()
        );

        $this->actingAs($this->user);

        $response = $this->get(route('products.getAll'));

        $response->assertStatus(200);
        $response->assertJsonStructure(['success', 'code', 'message', 'products']);
    }

    /**
     * @test
     */
    public function isPaginateWorking()
    {
        $this->instance(
            ProductServiceInterface::class,
            (App::make(ProductServiceMock::class))->generatePaginateWorking()
        );

        $this->actingAs($this->user);

        $response = $this->get(route('products.paginate'));

        $response->assertStatus(200);
        $response->assertJsonStructure(['success', 'code', 'message', 'products']);
    }

    /**
     * @test
     */
    public function isShowWorking()
    {
        $this->instance(
            ProductServiceInterface::class,
            (App::make(ProductServiceMock::class))->generateFindByIdWorking()
        );

        $this->actingAs($this->user);

        $response = $this->get(route('products.show', [1]));

        $response->assertStatus(200);
        $response->assertJsonStructure(['success', 'code', 'message', 'product']);
    }

    /**
     * @test
     */
    public function isStoreWorking()
    {
        $this->instance(
            ProductServiceInterface::class,
            (App::make(ProductServiceMock::class))->generateStoreWorking()
        );

        $this->actingAs($this->user);

        $response = $this->post(route('products.store'), [
            'name' => 'Category name',
            'description' => 'description',
            'barcode' => '123456',
            'price' => 2500000,
            'file' => UploadedFile::fake()->image('test.png'),
            'category_id' => 1
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure(['success', 'code', 'message', 'product_id']);
    }

    /**
     * @test
     */
    public function isUpdateWorking()
    {
        $this->instance(
            ProductServiceInterface::class,
            (App::make(ProductServiceMock::class))->generateUpdateWorking()
        );

        $this->actingAs($this->user);

        $response = $this->post(route('products.update', [1]), [
            'name' => 'Category name',
            'description' => 'description',
            'barcode' => '123456',
            'price' => 2500000,
            'category_id' => 1
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure(['success', 'code', 'message']);
    }

}
