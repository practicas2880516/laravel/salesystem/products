<?php

namespace Products\Application\Services;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Products\Application\Interfaces\Services\ProductServiceInterface;
use Products\Application\Mappers\Products\ProductNewDtoMapper;
use Products\Application\Mappers\Products\ProductUpdateDtoMapper;
use Products\Domain\Dto\Products\ProductNewDto;
use Products\Domain\Dto\Products\ProductUpdateDto;
use Products\Domain\Exceptions\ProductNotFoundException;
use Products\Infrastructure\Interfaces\Providers\Services\FileServiceProviderInterface;
use Products\Infrastructure\Interfaces\Repositories\Products\ProductRepositoryInterface;
use Products\Infrastructure\Providers\Dto\Files\FileNewDto;
use Products\Infrastructure\Providers\Dto\Files\FileUpdateDto;

class ProductService implements ProductServiceInterface
{
    /**
     * @var int
     */
    private int $productId;

    /**
     * @var object|null
     */
    private ?object $product;

    /**
     * @var FileServiceProviderInterface
     */
    private FileServiceProviderInterface $fileServiceProvider;

    /**
     * @var ProductRepositoryInterface
     */
    private ProductRepositoryInterface $productRepo;

    public function __construct(
        FileServiceProviderInterface $fileServiceProvider,
        ProductRepositoryInterface $productRepo
    )
    {
        $this->fileServiceProvider = $fileServiceProvider;
        $this->productRepo = $productRepo;
    }

    /**
     * @return int
     */
    public function getProductId():int
    {
        return $this->productId;
    }

    /**
     * @param int $id
     * @return object|null
     */
    public function findById(int $id):object|null
    {
        return $this->productRepo
            ->setUser(auth()->user())
            ->findById($id);
    }

    /**
     * @return Collection
     */
    public function getAll():Collection
    {
        return $this->productRepo
            ->setUser(auth()->user())
            ->getAll();
    }

    /**
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public function paginate(Request $request):LengthAwarePaginator
    {
        $recordsByPage = 20;
        $orderBy = 'created_at';
        $order = 'desc';

        switch ($request->get('search')) {
            case 'name':
                $this->productRepo->filterByName($request->get('value'));
                break;
            case 'category':
                $this->productRepo->filterByCategory($request->get('value'));
                break;
        }

        return $this->productRepo
            ->setUser(auth()->user())
            ->paginate($recordsByPage, $orderBy, $order);
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function store(Request $request): self
    {
        $dto = (App::make(ProductNewDtoMapper::class))
            ->createFromRequest($request);

        $dto->fileId = $this->storeFile($request->file);

        $this->storeProduct($dto);

        return $this;
    }

    /**
     * @param ProductNewDto $dto
     * @return self
     */
    public function storeProduct(ProductNewDto $dto):self
    {
        $this->productId = $this->productRepo
            ->setUser(auth()->user())
            ->store($dto)
            ->getProductId();

        return $this;
    }

    /**
     * @param UploadedFile $file
     * @return int
     */
    protected function storeFile(UploadedFile $file):int
    {
        $dto = new FileNewDto();
        $dto->document = $file;

        return $this->fileServiceProvider
            ->store($dto);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return $this
     * @throws \Throwable
     */
    public function update(Request $request, int $id):self
    {
        $this->setProductById($id);

        $dto = (App::make(ProductUpdateDtoMapper::class))
            ->updateFromRequest($request);

        $dto->id = $this->product->id;

        $this->updateProduct($dto);

        if (!is_null($request->file) && is_file($request->file)) {
            $this->updateFile($request->file, $dto->id);
        }

        return $this;
    }

    /**
     * @param ProductUpdateDto $dto
     * @return self
     */
    public function updateProduct(ProductUpdateDto $dto):self
    {
        $this->productRepo
            ->setUser(auth()->user())
            ->update($dto);

        return $this;
    }

    /**
     * @param UploadedFile $file
     * @param int $fileId
     * @return $this
     */
    protected function updateFile(UploadedFile $file, int $fileId):self
    {
        $dto = new FileUpdateDto();
        $dto->id = $fileId;
        $dto->document = $file;

        $this->fileServiceProvider
            ->update($dto);

        return $this;
    }

    /**
     * @param int $id
     * @return $this
     * @throws \Throwable
     */
    protected function setProductById(int $id):self
    {
        $this->product = $this->findById($id);

        throw_if(is_null($this->product), new ProductNotFoundException());

        return $this;
    }

}
