<?php

namespace Products\Application\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Products\Application\Interfaces\Services\CategoryServiceInterface;
use Products\Application\Mappers\Categories\CategoryUpdateDtoMapper;
use Products\Domain\Dto\Categories\CategoryNewDto;
use Products\Domain\Dto\Categories\CategoryUpdateDto;
use Products\Domain\Exceptions\CategoryNotFoundException;
use Products\Infrastructure\Interfaces\Repositories\Categories\CategoryRepositoryInterface;

class CategoryService implements CategoryServiceInterface
{
    /**
     * @var object|null
     */
    private ?object $category;

    /**
     * @var CategoryRepositoryInterface
     */
    private CategoryRepositoryInterface $categoryRepo;

    /**
     * @param CategoryRepositoryInterface $categoryRepo
     */
    public function __construct(
        CategoryRepositoryInterface $categoryRepo
    )
    {
        $this->categoryRepo = $categoryRepo;
    }

    /**
     * @return Collection
     */
    public function getAll():Collection
    {
        return $this->categoryRepo
            ->setUser(auth()->user())
            ->getAll();
    }

    /**
     * @param int $id
     * @return object|null
     */
    public function findById(int $id): object|null
    {
        return $this->categoryRepo
            ->setUser(auth()->user())
            ->findById($id);
    }


    /**
     * @param CategoryNewDto $dto
     * @return int
     */
    public function store(CategoryNewDto $dto):int
    {
        return $this->categoryRepo
            ->setUser(auth()->user())
            ->store($dto)
            ->getCategoryId();
    }

    /**
     * @param Request $request
     * @param int $id
     * @return $this
     * @throws \Throwable
     */
    public function update(Request $request, int $id):self
    {
        $this->setCategoryById($id);

        $dto = (App::make(CategoryUpdateDtoMapper::class))
            ->updateFromRequest($request);

        $dto->id = $this->category->id;

        $this->updateProduct($dto);

        return $this;
    }

    /**
     * @param CategoryUpdateDto $dto
     * @return $this
     */
    public function updateProduct(CategoryUpdateDto $dto):self
    {
        $this->categoryRepo
            ->setUser(auth()->user())
            ->update($dto);

        return $this;
    }

    /**
     * @param int $id
     * @return $this
     * @throws \Throwable
     */
    protected function setCategoryById(int $id):self
    {
        $this->category = $this->findById($id);

        throw_if(is_null($this->category), new CategoryNotFoundException());

        return $this;
    }
}
