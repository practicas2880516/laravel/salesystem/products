<?php

namespace Products\Application\Mappers\Products;

use Illuminate\Http\Request;
use Products\Application\Mappers\BaseMapper;
use Products\Domain\Dto\Products\ProductNewDto;

class ProductNewDtoMapper extends BaseMapper
{
    /**
     * @return ProductNewDto
     */
    protected function getNewDto(): ProductNewDto
    {
        return new ProductNewDto;
    }

    /**
     * @param Request $request
     * @return ProductNewDto
     */
    public function createFromRequest(Request $request):ProductNewDto
    {
        $dto = $this->getNewDto();
        $dto->name = $request->get('name');
        $dto->description = $request->get('description');
        $dto->comments = $request->get('comments');
        $dto->barcode = $request->get('barcode');
        $dto->price = $request->get('price');
        $dto->categoryId = $request->get('category_id');
        return $dto;
    }

}
