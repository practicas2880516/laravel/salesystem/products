<?php

namespace Products\Application\Mappers\Products;

use Illuminate\Http\Request;
use Products\Application\Mappers\BaseMapper;
use Products\Domain\Dto\Products\ProductUpdateDto;

class ProductUpdateDtoMapper extends BaseMapper
{
    protected function getNewDto(): ProductUpdateDto
    {
        return new ProductUpdateDto;
    }

    public function updateFromRequest(Request $request):ProductUpdateDto
    {
        $dto = $this->getNewDto();
        $dto->name = $request->get('name');
        $dto->description = $request->get('description');
        $dto->comments = $request->get('comments');
        $dto->barcode = $request->get('barcode');
        $dto->price = $request->get('price');
        $dto->categoryId = $request->get('category_id');
        return $dto;
    }

}
