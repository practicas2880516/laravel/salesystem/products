<?php

namespace Products\Application\Mappers\Categories;

use Illuminate\Http\Request;
use Products\Application\Mappers\BaseMapper;
use Products\Domain\Dto\Categories\CategoryUpdateDto;

class CategoryUpdateDtoMapper extends BaseMapper
{
    /**
     * @return CategoryUpdateDto
     */
    protected function getNewDto(): CategoryUpdateDto
    {
        return new CategoryUpdateDto;
    }

    /**
     * @param Request $request
     * @return CategoryUpdateDto
     */
    public function updateFromRequest(Request $request):CategoryUpdateDto
    {
        $dto = $this->getNewDto();
        $dto->name = $request->get('name');

        return $dto;
    }

}
