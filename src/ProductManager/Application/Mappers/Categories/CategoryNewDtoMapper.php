<?php

namespace Products\Application\Mappers\Categories;

use Illuminate\Http\Request;
use Products\Application\Mappers\BaseMapper;
use Products\Domain\Dto\Categories\CategoryNewDto;

class CategoryNewDtoMapper extends BaseMapper
{
    /**
     * @return CategoryNewDto
     */
    protected function getNewDto(): CategoryNewDto
    {
        return new CategoryNewDto;
    }

    /**
     * @param Request $request
     * @return CategoryNewDto
     */
    public function createFromRequest(Request $request):CategoryNewDto
    {
        $dto = $this->getNewDto();
        $dto->name = $request->get('name');

        return $dto;
    }

}
