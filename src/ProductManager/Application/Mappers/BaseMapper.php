<?php

namespace Products\Application\Mappers;

use Products\Domain\Dto\BaseDto;

abstract class BaseMapper
{
    /**
     * @var BaseDto
     */
    protected BaseDto $dto;

    /**
     * @return BaseDto
     */
    abstract protected function getNewDto():BaseDto;
}
