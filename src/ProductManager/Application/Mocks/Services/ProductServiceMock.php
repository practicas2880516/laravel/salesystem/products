<?php

namespace Products\Application\Mocks\Services;

use Illuminate\Pagination\LengthAwarePaginator;
use Products\Application\Interfaces\Services\ProductServiceInterface;

class ProductServiceMock
{
    public function generateGetAllWorking()
    {
        $mock = \Mockery::mock(ProductServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('getAll')
            ->once()
            ->andReturnUsing(function () {
                $product = new \stdClass();
                $product->id = 1;
                $product->name = 'Product name';

                return collect([$product]);
            });

        return $mock;
    }

    public function generatePaginateWorking()
    {
        $mock = \Mockery::mock(ProductServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('paginate')
            ->once()
            ->andReturnUsing(function () {
                $product = new \stdClass();
                $product->id = 1;
                $product->name = 'Product name';

                return new LengthAwarePaginator([$product], 1, 1, 1);
            });

        return $mock;
    }

    public function generateFindByIdWorking()
    {
        $mock = \Mockery::mock(ProductServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('findById')
            ->once()
            ->andReturnUsing(function () {
                $product= new \stdClass();
                $product->id = 1;
                $product->name = 'Product name';

                return $product;
            });

        return $mock;
    }

    public function generateStoreWorking()
    {
        $mock = \Mockery::mock(ProductServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('store')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('getProductId')
            ->once()
            ->andReturn(1);

        return $mock;
    }

    public function generateUpdateWorking()
    {
        $mock = \Mockery::mock(ProductServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('update')
            ->once()
            ->andReturnSelf();

        return $mock;
    }
}
