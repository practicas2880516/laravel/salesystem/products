<?php

namespace Products\Application\Mocks\Services;

use Products\Application\Interfaces\Services\CategoryServiceInterface;

class CategoryServiceMock
{
    public function generateGetAllWorking()
    {
        $mock = \Mockery::mock(CategoryServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('getAll')
            ->once()
            ->andReturnUsing(function () {
                $category= new \stdClass();
                $category->id = 1;
                $category->name = 'Category name';

                return collect([$category]);
            });

        return $mock;
    }

    public function generateFindByIdWorking()
    {
        $mock = \Mockery::mock(CategoryServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('findById')
            ->once()
            ->andReturnUsing(function () {
                $category= new \stdClass();
                $category->id = 1;
                $category->name = 'Category name';

                return $category;
            });

        return $mock;
    }

    public function generateStoreWorking()
    {
        $mock = \Mockery::mock(CategoryServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('store')
            ->once()
            ->andReturn(1);

        return $mock;
    }

    public function generateUpdateWorking()
    {
        $mock = \Mockery::mock(CategoryServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('update')
            ->once()
            ->andReturnSelf();

        return $mock;
    }
}
