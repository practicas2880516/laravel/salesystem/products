<?php

namespace Products\Application\Interfaces\Services;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Products\Domain\Dto\Products\ProductNewDto;
use Products\Domain\Dto\Products\ProductUpdateDto;

interface ProductServiceInterface
{
    /**
     * @return int
     */
    public function getProductId():int;

    /**
     * @param int $id
     * @return object|null
     */
    public function findById(int $id):object|null;

    /**
     * @return Collection
     */
    public function getAll():Collection;

    /**
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public function paginate(Request $request):LengthAwarePaginator;

    /**
     * @param Request $request
     * @return self
     */
    public function store(Request $request):self;

    /**
     * @param ProductNewDto $dto
     * @return self
     */
    public function storeProduct(ProductNewDto $dto):self;

    /**
     * @param Request $request
     * @param int $id
     * @return self
     */
    public function update(Request $request, int $id):self;

    /**
     * @param ProductUpdateDto $dto
     * @return self
     */
    public function updateProduct(ProductUpdateDto $dto):self;
}
