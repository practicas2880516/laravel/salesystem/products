<?php

namespace Products\Application\Interfaces\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Products\Domain\Dto\Categories\CategoryNewDto;
use Products\Domain\Dto\Categories\CategoryUpdateDto;

interface CategoryServiceInterface
{
    /**
     * @return Collection
     */
    public function getAll():Collection;

    /**
     * @param int $id
     * @return object|null
     */
    public function findById(int $id):object|null;

    /**
     * @param CategoryNewDto $dto
     * @return int
     */
    public function store(CategoryNewDto $dto):int;

    /**
     * @param Request $request
     * @param int $id
     * @return self
     */
    public function update(Request $request, int $id):self;

    /**
     * @param CategoryUpdateDto $dto
     * @return self
     */
    public function updateProduct(CategoryUpdateDto $dto):self;
}
