<?php

namespace Products\Infrastructure\Mocks\Repositories;

use Illuminate\Support\Str;
use Products\Infrastructure\Interfaces\Repositories\Categories\CategoryRepositoryInterface;

class CategoryRepositoryMock
{
    public function generateGetAllWorking()
    {
        $mock = \Mockery::mock(CategoryRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('getAll')
            ->once()
            ->andReturnUsing(function () {
                $category= new \stdClass();
                $category->id = 1;
                $category->name = Str::random(10);

                return collect([$category]);
            });

        return $mock;
    }

    public function generateFindByIdWorking()
    {
        $mock = \Mockery::mock(CategoryRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('findById')
            ->once()
            ->andReturnUsing(function () {
                $category= new \stdClass();
                $category->id = 1;
                $category->name = Str::random(10);

                return $category;
            });

        return $mock;
    }


    public function generateStoreWorking()
    {
        $mock = \Mockery::mock(CategoryRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('store')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('getCategoryId')
            ->once()
            ->andReturn(1);

        return $mock;
    }

    public function generateUpdateWorking()
    {
        $mock = \Mockery::mock(CategoryRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->times(2)
            ->andReturnSelf();

        $mock->shouldReceive('findById')
            ->once()
            ->andReturnUsing(function () {
                $category= new \stdClass();
                $category->id = 1;
                $category->name = Str::random(10);

                return $category;
            });

        $mock->shouldReceive('update')
            ->once()
            ->andReturnSelf();

        return $mock;
    }

    public function generateUpdateFailing()
    {
        $mock = \Mockery::mock(CategoryRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('findById')
            ->once()
            ->andReturnNull();

        return $mock;
    }

    public function generateUpdateProductWorking()
    {
        $mock = \Mockery::mock(CategoryRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('update')
            ->once()
            ->andReturnSelf();

        return $mock;
    }
}
