<?php

namespace Products\Infrastructure\Mocks\Repositories;

use Illuminate\Pagination\LengthAwarePaginator;
use Products\Infrastructure\Interfaces\Repositories\Products\ProductRepositoryInterface;

class ProductRepositoryMock
{
    public function generateGetAllWorking()
    {
        $mock = \Mockery::mock(ProductRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('getAll')
            ->once()
            ->andReturnUsing(function () {
                $product = new \stdClass();
                $product->id = 1;
                $product->name = 'Product name';
                return collect([$product]);
            });

        return $mock;
    }

    public function generatePaginateWorking()
    {
        $mock = \Mockery::mock(ProductRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('paginate')
            ->once()
            ->andReturnUsing(function () {
                $product = new \stdClass();
                $product->id = 1;
                $product->name = 'Product name';

                return new LengthAwarePaginator([$product], 1, 1, 1);
            });

        return $mock;
    }

    public function generateStoreProductWorking()
    {
        $mock = \Mockery::mock(ProductRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('store')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('getProductId')
            ->once()
            ->andReturn(1);

        return $mock;
    }

    public function generateUpdateWorking()
    {
        $mock = \Mockery::mock(ProductRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->times(2)
            ->andReturnSelf();

        $mock->shouldReceive('findById')
            ->once()
            ->andReturnUsing(function () {
                $obj = new \stdClass();
                $obj->id = 1;
                $obj->name = 'Product name';
                return $obj;
            });

        $mock->shouldReceive('update')
            ->once()
            ->andReturnSelf();

        return $mock;
    }

    public function generateUpdateFailing()
    {
        $mock = \Mockery::mock(ProductRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('findById')
            ->once()
            ->andReturnNull();

        return $mock;
    }

    public function generateUpdateProductWorking()
    {
        $mock = \Mockery::mock(ProductRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('update')
            ->once()
            ->andReturnSelf();

        return $mock;
    }
}
