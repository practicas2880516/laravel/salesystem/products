<?php

namespace Products\Infrastructure\Mocks\Providers\Services;

use Products\Infrastructure\Interfaces\Providers\Services\FileServiceProviderInterface;

class FileServiceProviderMock
{
    public function generateStoreWorking()
    {
        $mock = \Mockery::mock(FileServiceProviderInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('store')
            ->once()
            ->andReturn(1);

        return $mock;
    }

    public function generateUpdateWorking()
    {
        $mock = \Mockery::mock(FileServiceProviderInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('update')
            ->once()
            ->andReturnSelf();

        return $mock;
    }
}
