<?php

namespace Products\Infrastructure\Interfaces\Providers\Services;

use Products\Infrastructure\Providers\Dto\Files\FileNewDto;
use Products\Infrastructure\Providers\Dto\Files\FileUpdateDto;

interface FileServiceProviderInterface
{
    /**
     * @param FileNewDto $dto
     * @return int
     */
    public function store(FileNewDto $dto):int;

    /**
     * @param FileUpdateDto $dto
     * @return self
     */
    public function update(FileUpdateDto $dto):self;
}
