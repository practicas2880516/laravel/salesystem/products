<?php

namespace Products\Infrastructure\Interfaces\Repositories\Categories;

use Illuminate\Support\Collection;
use Products\Domain\Dto\Categories\CategoryNewDto;
use Products\Domain\Dto\Categories\CategoryUpdateDto;
use Products\Infrastructure\Interfaces\Repositories\BaseRepositoryInterface;

interface CategoryRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * @return Collection
     */
    public function getAll():Collection;

    /**
     * @param int $id
     * @return object|null
     */
    public function findById(int $id):object|null;

    /**
     * @return int
     */
    public function getCategoryId():int;

    /**
     * @param CategoryNewDto $dto
     * @return self
     */
    public function store(CategoryNewDto $dto):self;

    /**
     * @param CategoryUpdateDto $dto
     * @return self
     */
    public function update(CategoryUpdateDto $dto):self;
}
