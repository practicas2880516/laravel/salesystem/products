<?php

namespace Products\Infrastructure\Interfaces\Repositories\Products;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Products\Domain\Dto\Products\ProductNewDto;
use Products\Domain\Dto\Products\ProductUpdateDto;
use Products\Infrastructure\Interfaces\Repositories\BaseRepositoryInterface;

interface ProductRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * @return int
     */
    public function getProductId():int;

    /**
     * @param int $id
     * @return object|null
     */
    public function findById(int $id):object|null;

    /**
     * @return Collection
     */
    public function getAll():Collection;

    /**
     * @param int $recordsByPage
     * @param string $orderBy
     * @param string $order
     * @return LengthAwarePaginator
     */
    public function paginate(
        int $recordsByPage = 20, string $orderBy = 'created_at', string $order = 'desc'
    ):LengthAwarePaginator;

    /**
     * @param string $name
     * @return self
     */
    public function filterByName(string $name):self;

    /**
     * @param int $categoryId
     * @return self
     */
    public function filterByCategory(int $categoryId):self;

    /**
     * @param ProductNewDto $dto
     * @return self
     */
    public function store(ProductNewDto $dto):self;

    /**
     * @param ProductUpdateDto $dto
     * @return self
     */
    public function update(ProductUpdateDto $dto):self;
}
