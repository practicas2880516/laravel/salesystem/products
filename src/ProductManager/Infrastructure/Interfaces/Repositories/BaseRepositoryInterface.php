<?php

namespace Products\Infrastructure\Interfaces\Repositories;

interface BaseRepositoryInterface
{
    /**
     * @param object $user
     * @return self
     */
    public function setUser(object $user):self;

    /**
     * @return string
     */
    public function getTableName():string;

    /**
     * @return string
     */
    public function getDatabaseConnection():string;
}
