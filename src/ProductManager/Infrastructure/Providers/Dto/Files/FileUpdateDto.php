<?php

namespace Products\Infrastructure\Providers\Dto\Files;

use Illuminate\Http\UploadedFile;
use Products\Infrastructure\Providers\Dto\BaseDto;

class FileUpdateDto extends BaseDto
{
    /**
     * @var int
     */
    public int $id;

    /**
     * @var string|null
     */
    public ?string $name = null;

    /**
     * @var UploadedFile
     */
    public UploadedFile $document;
}
