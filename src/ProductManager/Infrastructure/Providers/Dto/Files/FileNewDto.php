<?php

namespace Products\Infrastructure\Providers\Dto\Files;

use Illuminate\Http\UploadedFile;
use Products\Infrastructure\Providers\Dto\BaseDto;

class FileNewDto extends BaseDto
{
    /**
     * @var string|null
     */
    public ?string $name = null;

    /**
     * @var UploadedFile
     */
    public UploadedFile $document;
}
