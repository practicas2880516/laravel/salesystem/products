<?php

namespace Products\Infrastructure\Providers\Services;

use Products\Infrastructure\Interfaces\Providers\Services\FileServiceProviderInterface;
use Products\Infrastructure\Providers\Dto\Files\FileNewDto;
use Products\Infrastructure\Providers\Dto\Files\FileUpdateDto;

class FileServiceProvider implements FileServiceProviderInterface
{
    /**
     * @var string
     */
    private string $fileStorageRoute;

    public function __construct()
    {
        $this->fileStorageRoute = env('FILE_STORAGE_HOST');
    }

    /**
     * @param FileNewDto $dto
     * @return int
     * @throws \Exception
     */
    public function store(FileNewDto $dto):int
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $this->fileStorageRoute.'/files/store',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => [
                'name' => $dto->name,
                'document' => $dto->document
            ],
            CURLOPT_HTTPHEADER => [
                'Accept: application/json',
                'Content-Type: application/json',
                'Authorization: Bearer ' . request()->bearerToken()
            ],
        ]);

        $response = json_decode(curl_exec($curl), true);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        if (curl_errno($curl) || $code !== 200)
            throw new \Exception($response['message'] ?? 'Server Error', $code);

        return $response['file_id'];
    }

    /**
     * @param FileUpdateDto $dto
     * @return $this
     * @throws \Exception
     */
    public function update(FileUpdateDto $dto):self
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $this->fileStorageRoute . '/files/' . $dto->id . '/update',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => [
                'name' => $dto->name,
                'document' => $dto->document
            ],
            CURLOPT_HTTPHEADER => [
                'Accept: application/json',
                'Content-Type: application/json',
                'Authorization: Bearer ' . request()->bearerToken()
            ],
        ]);

        $response = json_decode(curl_exec($curl), true);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        if (curl_errno($curl) || $code !== 200)
            throw new \Exception($response['message'] ?? 'Server Error', $code);

        return $this;
    }
}
