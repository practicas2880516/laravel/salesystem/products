<?php

namespace Products\Infrastructure\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Products\Application\Interfaces\Services\CategoryServiceInterface;
use Products\Application\Mappers\Categories\CategoryNewDtoMapper;
use Products\Domain\Dto\Categories\CategoryNewDto;

class CategoryController extends BaseController
{
    /**
     * @var CategoryServiceInterface
     */
    private CategoryServiceInterface $categoryService;

    /**
     * @param CategoryServiceInterface $categoryService
     */
    public function __construct(
        CategoryServiceInterface $categoryService
    )
    {
        $this->categoryService = $categoryService;
    }

    public function getAll()
    {
        return $this->executeWithJsonSuccessResponse(function () {
            return [
                'message' => 'Category list',
                'categories' => $this->categoryService->getAll()
            ];
        });
    }

    public function show(int $id)
    {
        return $this->executeWithJsonSuccessResponse(function () use ($id){
            return [
                'message' => 'Category',
                'category' => $this->categoryService->findById($id)
            ];
        });
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string']
        ]);

        return $this->executeWithJsonSuccessResponse(function () use ($request) {

            $dto = (App::make(CategoryNewDtoMapper::class))
                ->createFromRequest($request);

            return [
                'message' => 'Category created',
                'category_id' => $this->categoryService
                    ->store($dto)
            ];
        });
    }

    public function update(Request $request, int $id)
    {
        $request->validate([
            'name' => ['required', 'string']
        ]);

        return $this->executeWithJsonSuccessResponse(function () use ($request, $id) {

            $this->categoryService
                ->update($request, $id);

            return [
                'message' => 'Category created',
            ];
        });
    }
}
