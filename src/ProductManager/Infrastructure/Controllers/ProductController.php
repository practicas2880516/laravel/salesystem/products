<?php

namespace Products\Infrastructure\Controllers;

use Illuminate\Http\Request;
use Products\Application\Interfaces\Services\ProductServiceInterface;

class ProductController extends BaseController
{
    /**
     * @var ProductServiceInterface
     */
    private ProductServiceInterface $productService;

    /**
     * @param ProductServiceInterface $productService
     */
    public function __construct(
        ProductServiceInterface $productService
    )
    {
        $this->productService = $productService;
    }

    public function getAll()
    {
        return $this->executeWithJsonSuccessResponse(function () {
            return [
                'message' => 'Product List',
                'products' => $this->productService->getAll()
            ];
        });
    }

    public function paginate(Request $request)
    {
        return $this->executeWithJsonSuccessResponse(function () use ($request) {
            return [
                'message' => 'Product pagination',
                'products' => $this->productService->paginate($request)
            ];
        });
    }

    public function show(int $id)
    {
        return $this->executeWithJsonSuccessResponse(function () use ($id) {
            return [
                'message' => 'Product',
                'product' => $this->productService
                    ->findById($id)
            ];
        });
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'description' => ['required', 'string'],
            'comments' => ['nullable', 'string'],
            'barcode' => ['required', 'string'],
            'price' => ['required', 'numeric', 'gt:0'],
            'category_id' => ['required', 'integer'],
            'file' => ['required', 'file']
        ]);

        return $this->executeWithJsonSuccessResponse(function () use ($request) {
            return [
                'message' => 'Product created',
                'product_id' => $this->productService
                    ->store($request)
                    ->getProductId()
            ];
        });
    }

    public function update(Request $request, int $id)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'description' => ['required', 'string'],
            'comments' => ['nullable', 'string'],
            'barcode' => ['required', 'string'],
            'price' => ['required', 'numeric', 'gt:0'],
            'category_id' => ['required', 'integer'],
            'file' => ['nullable', 'file']
        ]);

        return $this->executeWithJsonSuccessResponse(function () use ($request, $id) {

            $this->productService
                ->update($request, $id);

            return [
                'message' => 'Updated product',
            ];
        });
    }
}
