<?php

namespace Products\Infrastructure\Controllers;

use App\Http\Controllers\Controller;

abstract class BaseController extends Controller
{
    /**
     * @param $callback
     * @return array|bool[]|\Illuminate\Http\JsonResponse|int[]|string[]
     */
    public function executeWithJsonSuccessResponse($callback)
    {
        try{
            $response = $callback();
            $response = array_merge([
                'success'=> true,
                'code'=> 200,
                'message'=> ''
            ], $response);
        }catch (\Exception $exception){
            report($exception);
            $code = (int) $exception->getCode() < 100 || $exception->getCode() > 599 ? 500 : $exception->getCode();
            $response = response()->json([
                'success'=> false,
                'code'=> $exception->getCode(),
                'message'=> $exception->getMessage()
            ], $code);
        }
        return $response;
    }
}
