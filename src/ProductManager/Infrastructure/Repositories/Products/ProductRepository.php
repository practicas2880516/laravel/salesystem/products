<?php

namespace Products\Infrastructure\Repositories\Products;

use Products\Domain\Dto\Products\ProductNewDto;
use Products\Domain\Dto\Products\ProductUpdateDto;
use Products\Infrastructure\Interfaces\Repositories\Products\ProductRepositoryInterface;
use Products\Infrastructure\Repositories\BaseRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class ProductRepository extends BaseRepository implements ProductRepositoryInterface
{
    private int $productId;

    /**
     * @var string
     */
    protected string $databaseConnection = 'pgsql';

    /**
     * @var string
     */
    protected string $tableName = 'products.products';

    /**
     * @return int
     */
    public function getProductId():int
    {
        return $this->productId;
    }

    /**
     * @param int $id
     * @return object|null
     */
    public function findById(int $id):object|null
    {
        return $this->query
            ->select([
                'products.id AS id',
                'products.name AS name',
                'products.description AS description',
                'products.comments AS comments',
                'products.barcode AS barcode',
                'products.price AS price',
                'products.file_id AS file_id',
                'products.category_id AS category_id',
                'categories.name AS category_name',
            ])
            ->leftJoin('products.categories', 'categories.id', '=', 'products.category_id')
            ->where('products.id', '=', $id)
            ->first();
    }

    /**
     * @return Collection
     */
    public function getAll():Collection
    {
        return $this->query
            ->select([
                'products.id AS id',
                'products.name AS name',
                'products.description AS description',
                'products.comments AS comments',
                'products.barcode AS barcode',
                'products.price AS price',
                'products.file_id AS file_id',
                'products.category_id AS category_id',
                'categories.name AS category_name',
            ])
            ->leftJoin('products.categories', 'categories.id', '=', 'products.category_id')
            ->orderBy('products.id', 'desc')
            ->get();
    }

    /**
     * @param int $recordsByPage
     * @param string $orderBy
     * @param string $order
     * @return LengthAwarePaginator
     */
    public function paginate(
        int $recordsByPage = 20, string $orderBy = 'created_at', string $order = 'desc'
    ):LengthAwarePaginator
    {
        return $this->query
            ->select([
                'products.id AS id',
                'products.name AS name',
                'products.description AS description',
                'products.comments AS comments',
                'products.barcode AS barcode',
                'products.price AS price',
                'products.file_id AS file_id',
                'products.category_id AS category_id',
                'categories.name AS category_name',
            ])
            ->leftJoin('products.categories', 'categories.id', '=', 'products.category_id')
            ->orderBy($this->tableName.'.'. $orderBy, $order)
            ->paginate($recordsByPage)
            ->appends(request()->query());
    }

    /**
     * @param string $name
     * @return $this
     */
    public function filterByName(string $name):self
    {
        $this->query
            ->where('products.name', 'ilike', '%' .$name.'%');

        return $this;
    }

    /**
     * @param int $categoryId
     * @return $this
     */
    public function filterByCategory(int $categoryId):self
    {
        $this->query
            ->where('products.category_id', '=', $categoryId);

        return $this;
    }

    /**
     * @param ProductNewDto $dto
     * @return $this
     */
    public function store(ProductNewDto $dto):self
    {
        $this->productId = $this->query
            ->insertGetId([
                'name' => $dto->name,
                'description' => $dto->description,
                'comments' => $dto->comments,
                'barcode' => $dto->barcode,
                'price' => $dto->price,
                'category_id' => $dto->categoryId,
                'file_id' => $dto->fileId,
                'created_at' => 'now()',
                'user_who_created_id' => $this->user->id ?? null
            ]);

        return $this;
    }

    /**
     * @param ProductUpdateDto $dto
     * @return $this
     */
    public function update(ProductUpdateDto $dto):self
    {
        $this->productId = $this->query
            ->where('id', '=', $dto->id)
            ->update([
                'name' => $dto->name,
                'description' => $dto->description,
                'comments' => $dto->comments,
                'barcode' => $dto->barcode,
                'price' => $dto->price,
                'category_id' => $dto->categoryId,
                'updated_at' => 'now()',
                'user_who_updated_id' => $this->user->id ?? null
            ]);

        return $this;
    }
}
