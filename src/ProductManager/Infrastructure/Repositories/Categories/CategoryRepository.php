<?php

namespace Products\Infrastructure\Repositories\Categories;

use Illuminate\Support\Collection;
use Products\Domain\Dto\Categories\CategoryNewDto;
use Products\Domain\Dto\Categories\CategoryUpdateDto;
use Products\Infrastructure\Interfaces\Repositories\Categories\CategoryRepositoryInterface;
use Products\Infrastructure\Repositories\BaseRepository;

class CategoryRepository extends BaseRepository implements CategoryRepositoryInterface
{
    /**
     * @var int
     */
    private int $categoryId;

    /**
     * @var string
     */
    protected string $databaseConnection = 'pgsql';

    /**
     * @var string
     */
    protected string $tableName = 'products.categories';

    /**
     * @return Collection
     */
    public function getAll():Collection
    {
        return $this->query
            ->select([
                'categories.id AS id',
                'categories.name AS name'
            ])
            ->orderBy('categories.id', 'desc')
            ->get();
    }

    public function findById(int $id): object|null
    {
        return $this->query
            ->select([
                'categories.id AS id',
                'categories.name AS name'
            ])
            ->where('categories.id', '=', $id)
            ->first();
    }


    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @param CategoryNewDto $dto
     * @return $this
     */
    public function store(CategoryNewDto $dto): self
    {
        $this->categoryId = $this->query
            ->insertGetId([
                'name' => $dto->name,
                'created_at' => 'now()',
                'user_who_created_id' => $this->user->id ?? null
            ]);
        return $this;
    }

    /**
     * @param CategoryUpdateDto $dto
     * @return $this
     */
    public function update(CategoryUpdateDto $dto): self
    {
        $this->query
            ->where('id', '=', $dto->id)
            ->update([
                'name' => $dto->name,
                'updated_at' => 'now()',
                'user_who_updated_id' => $this->user->id ?? null
            ]);
        return $this;
    }
}
