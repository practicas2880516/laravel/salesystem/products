<?php

namespace Products\Infrastructure\Repositories;

use Illuminate\Support\Facades\DB;
use Products\Infrastructure\Interfaces\Repositories\BaseRepositoryInterface;
use Illuminate\Database\Query\Builder;

abstract class BaseRepository implements BaseRepositoryInterface
{
    /**
     * @var Builder|null
     */
    protected ?Builder $query = null;

    /**
     * @var object|null
     */
    protected ?object $user = null;

    /**
     * @var string
     */
    protected string $tableName;

    /**
     * @var string
     */
    protected string $databaseConnection;

    public function __construct()
    {
        $this->setNewQuery();
    }

    /**
     * @return self
     */
    protected function setNewQuery():self
    {
        $this->query = DB::connection($this->databaseConnection)
            ->table($this->tableName);

        return $this;
    }

    /**
     * @param object $user
     * @return $this
     */
    public function setUser(object $user):self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableName():string
    {
        return $this->tableName;
    }

    /**
     * @return string
     */
    public function getDatabaseConnection():string
    {
        return $this->databaseConnection;
    }
}
