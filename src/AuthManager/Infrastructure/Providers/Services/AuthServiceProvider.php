<?php

namespace Auth\Infrastructure\Providers\Services;

use Auth\Infrastructure\Interfaces\Providers\Services\AuthServiceProviderInterface;

class AuthServiceProvider implements AuthServiceProviderInterface
{
    /**
     * @var string
     */
    private string $getUserRoute;

    public function __construct()
    {
        $this->getUserRoute = env('GET_USER_BY_TOKEN_ROUTE');
    }

    /**
     * @param string $token
     * @return array
     * @throws \Exception
     */
    public function getUserByToken(string $token):array
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $this->getUserRoute,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'Accept: application/json',
                'Content-Type: application/json',
                'Authorization: Bearer ' . $token
            ],
        ]);

        $response = json_decode(curl_exec($curl), true);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        if (curl_errno($curl) || $code !== 200)
            throw new \Exception($response['message'] ?? 'Server Error', $code);

        return $response['user'];
    }
}
