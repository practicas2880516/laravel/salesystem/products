<?php

namespace Auth\Infrastructure\Interfaces\Providers\Services;

interface AuthServiceProviderInterface
{
    /**
     * @param string $token
     * @return array
     */
    public function getUserByToken(string $token):array;
}
