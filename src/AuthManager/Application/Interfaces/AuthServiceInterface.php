<?php

namespace Auth\Application\Interfaces;

interface AuthServiceInterface
{
    /**
     * @param string $token
     * @return self
     */
    public function loginUserFromToken(string $token):self;
}
