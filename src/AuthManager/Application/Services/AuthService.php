<?php

namespace Auth\Application\Services;

use App\Models\User;
use Auth\Application\Interfaces\AuthServiceInterface;
use Auth\Infrastructure\Interfaces\Providers\Services\AuthServiceProviderInterface;

class AuthService implements AuthServiceInterface
{
    /**
     * @var AuthServiceProviderInterface
     */
    private AuthServiceProviderInterface $authServiceProvider;

    /**
     * @param AuthServiceProviderInterface $authServiceProvider
     */
    public function __construct(
        AuthServiceProviderInterface $authServiceProvider
    )
    {
        $this->authServiceProvider = $authServiceProvider;
    }

    /**
     * @param string $token
     * @return $this
     */
    public function loginUserFromToken(string $token):self
    {
        $userToken = $this->authServiceProvider
            ->getUserByToken($token);

        $user = new User();

        foreach ($userToken as $key => $value) {
            $user->{$key} = $value;
        }

        auth()->login($user);

        return $this;
    }
}
