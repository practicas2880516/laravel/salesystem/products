<?php

use Illuminate\Support\Facades\Route;
use Products\Infrastructure\Controllers\ProductController;
use Products\Infrastructure\Controllers\CategoryController;

Route::prefix('products')->group(function () {
    Route::get('get-all', [ProductController::class, 'getAll'])->name('products.getAll');
    Route::get('paginate', [ProductController::class, 'paginate'])->name('products.paginate');
    Route::post('store', [ProductController::class, 'store'])->name('products.store');
    Route::get('{id}/show', [ProductController::class, 'show'])->name('products.show');
    Route::post('{id}/update', [ProductController::class, 'update'])->name('products.update');
});

Route::prefix('categories')->group(function () {
    Route::get('get-all', [CategoryController::class, 'getAll'])->name('categories.getAll');
    Route::post('store', [CategoryController::class, 'store'])->name('categories.store');
    Route::get('{id}/show', [CategoryController::class, 'show'])->name('categories.show');
    Route::post('{id}/update', [CategoryController::class, 'update'])->name('categories.update');
});
